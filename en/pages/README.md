# Landing Pages

Pages within ActivDM are a powerful tool for quickly creating compelling content with a single focus. Use pages for directing contacts through a form or providing a way to download an asset, or merely tracking interest in a particular subject.

### Features of Landing Pages

There are many great features with ActivDM landing pages. These pages allow you to create an A/B testing environment (more on this later), multilingual pages, and templated pages unique to a variety of pre-defined templates.
